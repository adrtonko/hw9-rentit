package com.example.sales.web.controller;

import com.example.common.application.exceptions.PlantNotFoundException;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.service.InventoryService;
import com.example.invoice.application.dto.InvoiceDTO;
import com.example.invoice.domain.model.Invoice;
import com.example.invoice.service.InvoicingService;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.application.service.SalesService;
import com.example.sales.web.dto.CatalogQueryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("/dashboard")
public class DashboardController {
    @Autowired
    InventoryService inventoryService;
    @Autowired
    SalesService salesService;
    @Autowired
    InvoicingService invoicingService;




    @GetMapping("/catalog/form")
    public String getQueryForm(Model model) {
        model.addAttribute("catalogQuery", new CatalogQueryDTO());
        return "dashboard/catalog/query-form";
    }

    @GetMapping("/invoice")
    public String invoiceForm(Model model) {
        model.addAttribute("invoice", new Invoice());
        return "dashboard/invoices/invoice-form";
    }

    @PostMapping("/invoice")
    public String sendEmailInvoice(Model model, InvoiceDTO invoiceDTO) throws IOException, MessagingException {
        Invoice invoice = new Invoice();
        invoice.setPoHref(invoiceDTO.getPoHref());
        invoice.setAmount(invoiceDTO.getAmount());
        invoice.setDueDate(invoiceDTO.getDueDate());
        invoicingService.sendInvoice(invoice);
        //send it here
        return "redirect:/dashboard/invoice";
    }

    @PostMapping("/catalog/query")
    public String queryPlantCatalog(Model model, CatalogQueryDTO query) {
        List<PlantInventoryEntryDTO> plants = inventoryService.findAvailablePlants(
                query.getName(),
                query.getRentalPeriod().getStartDate(),
                query.getRentalPeriod().getEndDate()
        );
        model.addAttribute("plants", plants
                );
        PurchaseOrderDTO po = new PurchaseOrderDTO();
        po.setRentalPeriod(query.getRentalPeriod());
        model.addAttribute("po", po);
        return "dashboard/catalog/query-result";
    }

    @PostMapping("/orders")
    public String createPurchaseOrder(Model model, PurchaseOrderDTO purchaseOrderDTO) throws PlantNotFoundException {
        purchaseOrderDTO = salesService.createPurchaseOrder(purchaseOrderDTO);
        model.addAttribute("po", purchaseOrderDTO);
        return "redirect:/dashboard/orders/" + purchaseOrderDTO.get_id();
    }

    @GetMapping("/orders/{id}")
    public String showPurchaseOrder(Model model, @PathVariable String id) {
        PurchaseOrderDTO po = salesService.findPurchaseOrder(id);
        model.addAttribute("po", po);
        return "dashboard/orders/show";
    }
}
