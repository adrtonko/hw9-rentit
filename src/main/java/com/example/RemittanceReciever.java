package com.example;

import jdk.nashorn.internal.runtime.Context;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpMethod;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.core.Pollers;
import org.springframework.integration.dsl.http.Http;
import org.springframework.integration.dsl.mail.Mail;
import org.springframework.integration.dsl.scripting.Scripts;
import org.springframework.integration.dsl.support.Transformers;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * Created by Karl-Kristjan on 28.04.2017.
 */
@Configuration
public class RemittanceReciever {
    @Value("${gmail.username}")
    String gmailUsername;
    @Value("${gmail.password}")
    String gmailPassword;

    @Bean
    IntegrationFlow inBoundHttpGateway(){
        return IntegrationFlows.from(Http.inboundChannelAdapter("api/invoicing/remittance")
                .requestPayloadType(String.class)
                .crossOrigin(spec -> spec.origin("*"))

        ).channel("router-channel").get();
    }

    @Bean
    IntegrationFlow inboundMail() {
        return IntegrationFlows.from(Mail.imapInboundAdapter(
                String.format("imaps://%s:%s@imap.gmail.com:993/INBOX", gmailUsername, gmailPassword)
                ).selectorExpression("subject matches '.*remittance.*'"),
                e -> e.autoStartup(true)
                        .poller(Pollers.fixedDelay(20000))
        ).transform("@remittanceProcessor.extractRemittance(payload)")
                .channel("router-channel")
                .get();
    }

    @Bean
    IntegrationFlow router(){
        return IntegrationFlows
                .from("router-channel")
                .handle("remittanceProcessor", "processRemittance").get();
    }

}