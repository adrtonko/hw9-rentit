package com.example.invoice.service;

import com.example.invoice.domain.model.Invoice;
import com.example.invoice.domain.model.RemittanceAdvice;
import com.example.invoice.repository.InvoiceRepository;
import com.example.invoice.repository.RemittanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;

/**
 * Created by Karl-Kristjan on 04.05.2017.
 */
@Service
public class InvoicingService {
    @Autowired
    InvoicingGateway invoicingGateway;
    @Autowired
    RemittanceRepository remittanceRepository;
    @Autowired
    InvoiceRepository invoiceRepository;

    public void sendInvoice(Invoice invoice) throws IOException, MessagingException {
        invoice.setPaid(false);
        invoiceRepository.save(invoice);

        JavaMailSender mailSender = new JavaMailSenderImpl();
        String invoice1 = "{\n " +
                "\"poHref\": \"" + invoice.getPoHref() + "\", \n" +
                "\"amount\": "+ invoice.getAmount() + ", \n" +
                "\"dueDate\": \"" + invoice.getDueDate()+ "\"\n " +
                "}";
        MimeMessage rootMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(rootMessage, true);
        helper.setFrom("esihomework9@gmail.com");
        helper.setTo("esihomework9@gmail.com");
        helper.setSubject("invoice purchase order");
        helper.setText("plz give me money");
        helper.addAttachment("invoice-po-123.json", new ByteArrayDataSource(invoice1, "application/json"));


        RemittanceAdvice remittance = new RemittanceAdvice();
        remittance.setAmount(invoice.getAmount());
        remittance.setDueDate(invoice.getDueDate().toString());
        remittance.setPaid(false);
        remittance.setPoHref(invoice.getPoHref());
        remittanceRepository.save(remittance);

        invoicingGateway.sendInvoice(rootMessage);
    }
}

