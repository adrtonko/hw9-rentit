package com.example.invoice.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.mail.Mail;

/**
 * Created by Karl-Kristjan on 03.05.2017.
 */
@Configuration
public class EmailSender {

    @Value("${gmail.username}")
    String gmailUsername;

    @Value("${gmail.password}")
    String gmailPassword;
    //Create Invoice
    //
    //JPArepository
    @Bean
    IntegrationFlow sendEmailInvoice(){
        return IntegrationFlows.from("sendInvoiceChannel")
                .handle(Mail.outboundAdapter("smtp.gmail.com")
                        .port(465)
                        .protocol("smtps")
                        .credentials(gmailUsername, gmailPassword)
                        .javaMailProperties(p->p.put("mail.debug", "false"))).get();
    }
    /*
    @Configuration
    public class InvoicingFlow {
        @Bean
        IntegrationFlow sendInvoice() {
            return IntegrationFlows.from("sendInvoice-http-channel")
                    // Add here your solution (adapt one of the integration flows
                    // introduced during the lecture/lab sessions)
                    .handle(System.out::println)
                    .get();
        }
    }*/
    //CreateInvoice via web-form -> first we need a web-form


}
