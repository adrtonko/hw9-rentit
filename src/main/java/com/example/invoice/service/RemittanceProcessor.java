package com.example.invoice.service;

import com.example.invoice.domain.model.Invoice;
import com.example.invoice.domain.model.RemittanceAdvice;
import com.example.invoice.repository.InvoiceRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;

import javax.mail.BodyPart;
import javax.mail.Multipart;
import javax.mail.internet.MimeMessage;
import java.io.IOException;

@Service
class RemittanceProcessor {

    @Autowired
    InvoiceRepository invoiceRepository;

    public String extractRemittance(MimeMessage msg) throws Exception {
        System.out.println(msg);
        Multipart multipart = (Multipart) msg.getContent();
        for (int i = 0; i < multipart.getCount(); i++) {
            BodyPart bodyPart = multipart.getBodyPart(i);
            if (bodyPart.getContentType().contains("json") &&
                    bodyPart.getFileName().startsWith("remittance"))
                return IOUtils.toString(bodyPart.getInputStream(), "UTF-8");
        }
        throw new Exception("oops");
    }

    public void processRemittance(Message<String> remittance) throws IOException {
        String invoicePayload = remittance.getPayload();

        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModules(new JavaTimeModule());//this might not be nessesary

        RemittanceAdvice remittanceAdvice = mapper.readValue(invoicePayload, RemittanceAdvice.class);
        Invoice saved = invoiceRepository.findOneByPoHref(remittanceAdvice.getPoHref());

        if (saved!= null) {
            saved.setPaid(true);
            invoiceRepository.save(saved);
        }

    }
}

