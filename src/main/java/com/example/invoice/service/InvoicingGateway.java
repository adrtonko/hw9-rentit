package com.example.invoice.service;

import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;


import javax.mail.internet.MimeMessage;
/**
 * Created by Karl-Kristjan on 03.05.2017.
 */
@MessagingGateway
public interface InvoicingGateway {
    @Gateway(requestChannel = "sendInvoiceChannel")
    public void sendInvoice(MimeMessage msg);
}



