package com.example.invoice.service;

import com.example.invoice.domain.model.Invoice;
import com.example.invoice.repository.InvoiceRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

@Component
public class InvocesNotPaydScheduledService {

    private static final Logger log = LoggerFactory.getLogger(InvocesNotPaydScheduledService.class);

    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    InvoiceRepository invoiceRepository;

    @Autowired
    InvoicingService invoicingService;

//    @Scheduled(fixedRate = 5000)
    @Scheduled(cron = "0 0 18 * * FRI")
    public void reportCurrentTime() {
        log.info("Sending not payd invoices {}", dateFormat.format(new Date()));

        LocalDate now = LocalDate.now();

        for (Invoice invoice : invoiceRepository.findNotPayedInvoices(now)) {
            try {
                log.info("Send invoice: {}", invoice);
                invoicingService.sendInvoice(invoice);

            } catch (Exception e) {
                e.printStackTrace();
                log.error("Sending invoice failed: " + invoice, e);
            }
        }

    }
}