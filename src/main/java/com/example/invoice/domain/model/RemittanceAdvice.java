package com.example.invoice.domain.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Data
public class RemittanceAdvice {
    @Id @GeneratedValue
    Long id;

    String poHref;
    BigDecimal amount;
    String dueDate;

    boolean paid;
}
