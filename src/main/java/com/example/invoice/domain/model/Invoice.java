package com.example.invoice.domain.model;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by Karl-Kristjan on 28.04.2017.
 */
@Data
@Entity
public class Invoice {
    @Id @GeneratedValue
    Long id;

    String poHref;
    BigDecimal amount;
    LocalDate dueDate;
    boolean paid;
}


