package com.example.invoice.repository;

import com.example.invoice.domain.model.Invoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by Karl-Kristjan on 03.05.2017.
 */
@Repository
public interface InvoiceRepository extends JpaRepository<Invoice, String> {

    @Query("select i " +
            "from Invoice i " +
            "where dueDate < ? " +
            "order by dueDate")
    List<Invoice> findNotPayedInvoices(LocalDate datum);

    Invoice findOneByPoHref(String poHref);
}
