package com.example.invoice.repository;

import com.example.invoice.domain.model.RemittanceAdvice;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by denis on 4.05.17.
 */
public interface RemittanceRepository extends JpaRepository<RemittanceAdvice, Long> {

    RemittanceAdvice findByPoHref(String poHref);
}
