package com.example.invoice.application.dto;

import com.example.common.rest.ResourceSupport;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * Created by Karl-Kristjan on 03.05.2017.
 */

@Data
public class InvoiceDTO {
    String poHref;
    BigDecimal amount;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    LocalDate dueDate;
}
